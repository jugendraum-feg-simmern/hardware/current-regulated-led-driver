**THIS IS STILL WORK IN PROGRESS AND NOTHING IS TESTED YET**

# current regulated led driver
A constant current power supply based upon the [LM3409HV](datasheets/LM3409-TI.pdf), which does all the regulation and switching of the power mosfet. An ATTiny202 is used to interface with a controller via UART / RS485 and provides a control voltage to the LM3409 adjusting the current setpoint. Additionally, current, voltage and temperature are monitored and can also be read from the ATTiny202.

The driver is designed for an input voltage of 48 V and can provide up to 8 A to an LED array.

![render](doc/3d_render.png)
