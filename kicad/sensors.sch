EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Sensor block"
Date "2021-04-07"
Rev "1.1"
Comp ""
Comment1 "    Tres = 0.5 °C/LSB"
Comment2 "    Ures = 50 mV/LSB"
Comment3 "    Ires =  12 mA/LSB"
Comment4 "Resolutions with a 10 bit ADC:"
$EndDescr
Text HLabel 2900 1750 2    50   Input ~ 0
CSENSp
Text HLabel 2600 2450 2    50   Input ~ 0
CSENSn
$Comp
L Amplifier_Current:AD8217 U4
U 1 1 605D7B2B
P 1950 2250
F 0 "U4" V 1483 2250 50  0000 C CNN
F 1 "AD8217" V 1574 2250 50  0000 C CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 1950 2250 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8217.pdf" H 2600 1550 50  0001 C CNN
	1    1950 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 2450 2350 2450
Text HLabel 1250 2950 0    50   Output ~ 0
IMON
Text Notes 3300 2100 0    50   ~ 0
Max 250 mV differential voltage across inputs of AD8217\n  -> gain = 20 leads to 5V @ max U_diff\n  -> 10 A / 250 mV = 25 mOhm maximal
Wire Wire Line
	1950 2650 1950 2950
Wire Wire Line
	1950 2950 1250 2950
$Comp
L power:GND #PWR0123
U 1 1 605DA17C
P 1450 2350
F 0 "#PWR0123" H 1450 2100 50  0001 C CNN
F 1 "GND" H 1455 2177 50  0000 C CNN
F 2 "" H 1450 2350 50  0001 C CNN
F 3 "" H 1450 2350 50  0001 C CNN
	1    1450 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2350 1450 2250
Wire Wire Line
	1450 2250 1550 2250
$Comp
L Sensor_Temperature:TC1047AxNB U3
U 1 1 605DB148
P 1800 5950
F 0 "U3" H 1470 5996 50  0000 R CNN
F 1 "TC1047AxNB" H 1470 5905 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1800 5550 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21498D.pdf" H 1650 6200 50  0001 C CNN
	1    1800 5950
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 605DDBB5
P 1800 6300
F 0 "#PWR0124" H 1800 6050 50  0001 C CNN
F 1 "GND" H 1805 6127 50  0000 C CNN
F 2 "" H 1800 6300 50  0001 C CNN
F 3 "" H 1800 6300 50  0001 C CNN
	1    1800 6300
	1    0    0    -1  
$EndComp
Text HLabel 1650 5550 0    50   Input ~ 0
5V
Wire Wire Line
	1650 5550 1800 5550
Wire Wire Line
	1800 5550 1800 5650
$Comp
L power:+5V #PWR0125
U 1 1 605DE2AC
P 1800 5450
F 0 "#PWR0125" H 1800 5300 50  0001 C CNN
F 1 "+5V" H 1815 5623 50  0000 C CNN
F 2 "" H 1800 5450 50  0001 C CNN
F 3 "" H 1800 5450 50  0001 C CNN
	1    1800 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 5450 1800 5550
Connection ~ 1800 5550
Text HLabel 1100 5950 0    50   Output ~ 0
TMON
Wire Wire Line
	1100 5950 1400 5950
Text Notes 3300 2500 0    50   ~ 0
10 bit ADC\n  -> U_res = 5 V / 2^10 = 4.88 mV/LSB\nwith gain A = 20 of AD8217 and 20 mOhm R_SENS\n  -> I_res = U_res / (A * R_SENS) = 12.2 mA/LSB
Text HLabel 7500 1700 1    50   Output ~ 0
VLED
Wire Wire Line
	7500 2100 7500 2200
$Comp
L power:GND #PWR0126
U 1 1 605E6038
P 7500 2700
F 0 "#PWR0126" H 7500 2450 50  0001 C CNN
F 1 "GND" H 7505 2527 50  0000 C CNN
F 2 "" H 7500 2700 50  0001 C CNN
F 3 "" H 7500 2700 50  0001 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2600 7500 2700
Wire Wire Line
	7500 1700 7500 1800
Wire Wire Line
	7500 2200 7000 2200
Connection ~ 7500 2200
Wire Wire Line
	7500 2200 7500 2300
Text HLabel 7000 2200 0    50   Output ~ 0
UMON
$Comp
L Device:R R9
U 1 1 605E4AD2
P 7500 2450
F 0 "R9" H 7570 2496 50  0000 L CNN
F 1 "10k" H 7570 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7430 2450 50  0001 C CNN
F 3 "~" H 7500 2450 50  0001 C CNN
	1    7500 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 605E472C
P 7500 1950
F 0 "R8" H 7570 1996 50  0000 L CNN
F 1 "90k" H 7570 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7430 1950 50  0001 C CNN
F 3 "~" H 7500 1950 50  0001 C CNN
	1    7500 1950
	1    0    0    -1  
$EndComp
Text Notes 7900 2200 0    50   ~ 0
U_mon = V_LED / 10\n  -> resolution: (5 V / 2^10) * 10 = 48.8 mV / LSB 
Wire Bus Line
	5800 800  5800 7650
Wire Bus Line
	550  4100 11100 4100
Text Notes 2900 5550 0    50   ~ 0
TC1047 : output 10 mV / °C\n  -> ATTiny : 4.88 mV / LSB\n  -> resolution: 0.488 °C / LSB
$Comp
L Device:C_Small C9
U 1 1 605F7EE3
P 1000 5050
F 0 "C9" H 1092 5096 50  0000 L CNN
F 1 "100n" H 1092 5005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1000 5050 50  0001 C CNN
F 3 "~" H 1000 5050 50  0001 C CNN
	1    1000 5050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0127
U 1 1 605F87C3
P 1000 4900
F 0 "#PWR0127" H 1000 4750 50  0001 C CNN
F 1 "+5V" H 1015 5073 50  0000 C CNN
F 2 "" H 1000 4900 50  0001 C CNN
F 3 "" H 1000 4900 50  0001 C CNN
	1    1000 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 605F8B75
P 1000 5200
F 0 "#PWR0128" H 1000 4950 50  0001 C CNN
F 1 "GND" H 1005 5027 50  0000 C CNN
F 2 "" H 1000 5200 50  0001 C CNN
F 3 "" H 1000 5200 50  0001 C CNN
	1    1000 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 6250 1800 6300
Wire Wire Line
	1000 4900 1000 4950
Wire Wire Line
	1000 5150 1000 5200
$Comp
L Device:C_Small C10
U 1 1 60587160
P 2650 1900
F 0 "C10" H 2742 1946 50  0000 L CNN
F 1 "100n" H 2742 1855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2650 1900 50  0001 C CNN
F 3 "~" H 2650 1900 50  0001 C CNN
	1    2650 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0144
U 1 1 6058792C
P 2650 2050
F 0 "#PWR0144" H 2650 1800 50  0001 C CNN
F 1 "GND" H 2655 1877 50  0000 C CNN
F 2 "" H 2650 2050 50  0001 C CNN
F 3 "" H 2650 2050 50  0001 C CNN
	1    2650 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 2050 2450 2050
Wire Wire Line
	2450 2050 2450 1750
Wire Wire Line
	2450 1750 2650 1750
Wire Wire Line
	2650 1750 2650 1800
Wire Wire Line
	2650 2000 2650 2050
Wire Wire Line
	2650 1750 2900 1750
Connection ~ 2650 1750
$EndSCHEMATC
