#!/usr/bin/env python3

################################################################################
# Calculate nominal values for the supplemental components to the LM3409HV when
# used in the typical application as depicted on the first page of the datasheet
#
# The formulas and order of calculation closely follows the design guide
# given in the datasheet from page 20 onward.
################################################################################

import numpy as np
from math import floor, log10

from engineering_notation import EngNumber

class specs:
    """
    input 'struct' with parameters that influence the choice of components.
    Some more will be calculated and added on the way.
    """
    V_in = 48  # V           -> Nominal input voltage
    V_o = 35  # V            -> Nominal output voltage (# of LEDs x forward voltage)
    r_d = 1  # Ohm           -> LED string dynamic resistance
    f_sw = 500e3  # Hz       -> Switching frequency (at nominal v_in, v_o)
    I_led = 8  # A           -> Average LED current
    dI_L1_pp = 1  # A      -> Inductor current ripple
    dI_led_pp = 1  # A     -> LED current ripple
    dV_in_pp = 0.2  # V      -> Input voltage ripple
    V_to, V_hys = 40, 5  # V -> Under voltage lockout (UVLO) characteristics
    eta = 0.95  # unity      - > Expected efficiency

class comp:
    """
    output 'struct' with the component values
    filled with starting values for some components, but will be mainly filled by calculations
    """
    C_off = 470e-12


def _requires(*args):
    """
    helper function to assert requirements are met, i.e. parameters are not None"
    """
    for a in args:
        assert hasattr(comp, a) or hasattr(specs, a), f"calculate {a} first"

def matchE96(c):
    """
    update the value of the component to the nearest match in the E96 series
    """
    def fexp(x):
        return int(floor(log10(abs(x)))) if x != 0 else 0
    def fman(x):
        return x/10**fexp(x)
    def closest(lst, K):
        return lst[min(range(len(lst)), key = lambda i: abs(lst[i]-K))]

    # https://en.wikipedia.org/wiki/E_series_of_preferred_numbers#Lists
    e96 = [1.00, 1.02, 1.05, 1.07, 1.10, 1.13, 1.15, 1.18, 1.21, 1.24, 1.27, 1.30, 1.33, 1.37, 1.40, 1.43, 1.47, 1.50, 1.54, 1.58, 1.62, 1.65, 1.69, 1.74, 1.78, 1.82, 1.87, 1.91, 1.96, 2.00, 2.05, 2.10, 2.15, 2.21, 2.26, 2.32, 2.37, 2.43, 2.49, 2.55, 2.61, 2.67, 2.74, 2.80, 2.87, 2.94, 3.01, 3.09, 3.16, 3.24, 3.32, 3.40, 3.48, 3.57, 3.65, 3.74, 3.83, 3.92, 4.02, 4.12, 4.22, 4.32, 4.42, 4.53, 4.64, 4.75, 4.87, 4.99, 5.11, 5.23, 5.36, 5.49, 5.62, 5.76, 5.90, 6.04, 6.19, 6.34, 6.49, 6.65, 6.81, 6.98, 7.15, 7.32, 7.50, 7.68, 7.87, 8.06, 8.25, 8.45, 8.66, 8.87, 9.09, 9.31, 9.53, 9.76]

    man, exp = fman(c), fexp(c)
    man = closest(e96, man)
    return man * 10**exp

def calc_R_off():
    """
    Calculate switching frequency (f_sw) at the nominal operating point (V_in and V_o).
    Assume a C_off value (between 470pF and 1nF) and a system efficiency (eta).
    Solve for R_off
    """
    comp.R_off = -(1 - specs.V_o / (specs.eta * specs.V_in)) / ((comp.C_off + 20e-12) * specs.f_sw * np.log(1-1.24 / specs.V_o))
    print(f"R_off = {EngNumber(comp.R_off)}Ohm")

def calc_t_off():
    """
    Once R_off is known, t_off can be calculated
    """
    _requires('R_off')
    specs.t_off = -(comp.C_off + 20e-12) * comp.R_off * np.log(1 - 1.24 / specs.V_o)
    print(f"t_off = {EngNumber(specs.t_off)}s")

def recalc_f_sw():
    """
    Once R_off is known, f_sw can be calculated.
    This is useful when R_off was picked to be from an E series rather than the
    calculated one.
    """
    _requires('R_off', 't_off')
    specs.f_sw = (1 - specs.V_o/(specs.eta * specs.V_in)) / specs.t_off
    print(f"recalculated f_sw = {EngNumber(specs.f_sw)}Hz")

def calc_L1():
    """
    Set the inductor ripple current (dI_l_pp) by solving for the appropriate inductor (L1)
    """
    _requires('t_off')
    comp.L1 = specs.V_o * specs.t_off / specs.dI_L1_pp
    print(f"L1 = {EngNumber(comp.L1)}H")

def recalc_dI_L1_pp():
    """
    Once L1 has been calculated, the inductor ripple current can be recalculated.
    This is useful when L1 was picked to be from an E series rather than the
    calculated one.
    """
    _requires('L1', 't_off')
    specs.dI_L1_pp = specs.V_o * specs.t_off / comp.L1
    print(f"recalculated dI_L1_pp = {EngNumber(specs.dI_L1_pp)}A")

def calc_I_L1_max():
    """
    Set the average LED current (I_led) by first solving for the peak inductor current (I_L1_max)
    """
    specs.I_L1_max = specs.I_led + specs.dI_L1_pp / 2
    print(f"I_L1_max = {EngNumber(specs.I_L1_max)}A")

def calc_R_sns():
    """
    Peak inductor current is detected across the sense resistor (R_sns).
    In most cases, assume the maximum value (V_adj = 1.24V) at the IADJ pin and solve for R_sns
    """
    specs.V_adj = 1.24
    _requires('V_adj', 'I_L1_max')
    comp.R_sns = specs.V_adj / (5 * specs.I_L1_max)
    print(f"R_sns = {EngNumber(comp.R_sns)}Ohm")

def recalc_I_led():
    """
    Once R_sns has been calculated, the led current can be recalculted.
    This is useful when R_sns was picked to be from an E series rather than the
    calculated one.
    """
    _requires('R_sns', 'V_adj')
    specs.I_led = specs.V_adj / (5 * comp.R_sns) - specs.dI_L1_pp / 2
    print(f"recalculated I_led = {EngNumber(specs.I_led)}A")

def calc_R_uv():
    """
    Input UVLO is set with the turn-on threshold voltage (V_to) and the desired hysteresis (V_hys).
    To set V_hys, solve for RUV2
    """
    comp.R_uv2 = specs.V_hys / 22e-6
    print(f"R_uv2 = {EngNumber(comp.R_uv2)}Ohm")
    comp.R_uv1 = 1.24 * comp.R_uv2 / (specs.V_hys - 1.24)
    print(f"R_uv1 = {EngNumber(comp.R_uv1)}Ohm")

if __name__ == '__main__':
    calc_R_off()
    comp.R_off = matchE96(comp.R_off);print(f"matched R_off = {EngNumber(comp.R_off)}Ohm")
    calc_t_off()
    recalc_f_sw()
    calc_L1()
    comp.L1 = matchE96(comp.L1);comp.L1 = 100e-6; print(f"matched L1 = {EngNumber(comp.L1)} H")
    recalc_dI_L1_pp()
    calc_I_L1_max()
    calc_R_sns()
    comp.R_sns = matchE96(comp.R_sns);print(f"matched R_sns = {EngNumber(comp.R_sns)} Ohm")
    recalc_I_led()
    calc_R_uv()
